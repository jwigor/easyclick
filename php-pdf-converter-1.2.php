<?
  //This script runs only with a ghostscript installation and an installed postscript printer.
  //copy the script into the Apache htdocs-Directory and edit the lines 9 and 10 below.

  //If it don`t runs as expected please comment out the lines 173, 187, 188, 191, 192 which
  //deletes the postscriptfiles and uploaded files after conversion process. Then you can see whether
  //the upload was successfull or the postscriptfiles will be generated from the script

  //This Script is for free use and copy. For updates, information and help see http://www.goermezer.de
  // --> http://www.goermezer.de
  //Please only edit the next 2 variables !
  //Path to Ghostscript Executable
  $gsinst="C:\\ServPDF\\spyce\\gs\\gs8.14\\bin\\gswin32c.exe";
  //Postscript Printername (you can download an excellent Postscript Driver Archive from http://www.goermezer.de
  $printername="ServPDF";

  //Supported File Formats. There are more formats supported from Office. If you know them, edit the next lines...
  $quality = $_POST['quality'];
  $excelfiles = array ("xls", "XLS", "xlt", "XLT", "csv", "CSV");
  $powerpointfiles = array('ppt', 'PPT', 'pot', 'POT');
  $wordfiles = array("doc", "DOC", 'dot', 'DOT', 'rtf', 'RTF', 'tml', 'TML', 'htm', 'HTM', 'txt', 'TXT');
  $postscriptfiles = array('.ps', '.PS', 'prn', 'PRN', 'eps', 'EPS');

  //f you are not familiar with PHP, do nothing else from here !
  error_reporting(E_ALL ^ E_NOTICE);
  $servername = $HTTP_SERVER_VARS['SERVER_NAME'];
  $workdir = getcwd()."\\"; //erzeugt C:\\Apache2\\htdocs\\
  $psfile=$workdir.$_FILES['userfile']['name'].".ps";
  $pdffile=$workdir.$_FILES['userfile']['name'].".pdf";
  $uploaded_doc=$workdir.$_FILES['userfile']['name'];
?>
<html><head><title>PDF Document Converter</title></head>
<body bgcolor="#FFFFFF" text="#000000" link="#0000FF">
<table width="100%" border="0">
  <tr>
    <td width="94%"><b>Office2PDF Conversion Service</b></td>
  </tr>
  <tr>
    <td>
      <? echo "Supported File Endings: <br>" ?>
      <? foreach ($wordfiles as $value) {
          echo "$value, ";
          }
          foreach ($excelfiles as $value) {
          echo "$value, ";
          }
          foreach ($powerpointfiles as $value) {
          echo "$value, ";
          }
          foreach ($postscriptfiles as $value) {
          echo "$value, ";
          }
        ?>
    </td>
  </tr>
</table>
<form enctype="multipart/form-data" action="<?=$PHP_SELF ?>" method="post">
<table width="100%" border="0" bgcolor="#CCCCCC">
  <tr>
    <td width="17%">Office File: </td>
    <td width="83%"><input name="userfile" type="file"></td>
  </tr>
  <tr>
    <td>Quality: </td>
    <td><table width="800">
      <tr>
        <td><label>
          <input type="radio" checked name="quality" value="-dPDFSETTINGS=/default">
          Default: Standard setting, middle sized PDFs, Acrobat 4 compatible</label>
        </td>
      </tr>
      <tr>
        <td><label>
          <input type="radio" name="quality" value="-dPDFSETTINGS=/screen">
          Screen: small PDFs, especially for Internet, Acrobat 3 compatible</label>
        </td>
      </tr>
      <tr>
        <td><label>
          <input type="radio" name="quality" value="-dPDFSETTINGS=/ebook">
          EBook: PDFs especially for e-Books (= middle Quality), Acrobat 4 compatible</label>
        </td>
      </tr>
      <tr>
        <td><label>
            <input type="radio" name="quality" value="-dPDFSETTINGS=/printer">
            Printer: PDFs especially for Printing Documents (= good Quality), Acrobat 4 compatible</label>
          </td>
      </tr>
      <tr>
        <td><label>
          <input type="radio" name="quality" value="-dPDFSETTINGS=/prepress">
          Prepress: PDFs especially for die Printing (= best Quality). Acrobat 4 compatible</label>
        </td>
      </tr>
    </table>
    </td>
    </tr>
  <tr>
    <td>Start conversion</td>
    <td><input type="submit" name="send" value="send file">
    </td>
  </tr>
</table>
</form>
<?
//If Document has Password, you can use this variable (works not with Powerpoint
if ($_POST['password']){
  $password=$_POST['password'];
  }
  else { $password="False"; }
list ($name, $suffix) = split ('[.]', $_FILES['userfile']['name']);
function excel($document, $ps_file, $pdf_file, $gs_inst, $printer_name, $out_quality){
  $excel = new COM("excel.application") or die("Unable to instantiate Excel");
  $excel->AskToUpdateLinks = 0;
  $excel->Workbooks->Open($document) or die("Unable to open document");
  $excel->Workbooks[1]->Saved=1;
  $excel->Workbooks[1]->PrintOut(1, 5000, 1, False, $printer_name, True, False, $ps_file);
  $excel->Workbooks[1]->Close(false);
  $excel->Quit();
  $excel = null;
  unset($excel);
  while (exec("$gs_inst -sDEVICE=pdfwrite -r300 $out_quality -dNOPAUSE -dBATCH -dSAFER -sPAPERSIZE=a4 -sOutputFile=\"".$pdf_file."\" \"".$ps_file."\"") > 0){
    sleep(1);
    }
  }
function powerpoint($document, $ps_file, $pdf_file, $gs_inst, $printer_name, $out_quality){
  $powerpoint = new COM("powerpoint.application") or die("Unable to instantiate Powerpoint");
  $powerpoint->Visible = 1;
  $powerpoint->Presentations->Open($document, False, False, False) or die("Unable to open document");
  $powerpoint->Presentations[1]->Saved=1;
  //$powerpoint->ActivePrinter = $printer_name;
  $powerpoint->Presentations[1]->PrintOut(1, 5000, $ps_file, 0, False);
  $powerpoint->Presentations[1]->Close();
  $powerpoint->Quit();
  $powerpoint = null;
  //unset($powerpoint);
  while (exec("$gs_inst -sDEVICE=pdfwrite -r300 ".$out_quality." -dNOPAUSE -dBATCH -dSAFER -sPAPERSIZE=a4 -sOutputFile=\"".$pdf_file."\" \"".$ps_file."\"") > 0) {
    sleep(1);
    }
  }
function word($document, $ps_file, $pdf_file, $passwd, $gs_inst, $printer_name, $out_quality){
  $word = new COM("word.application") or die("Unable to instantiate Word");
  //$word->Visible = 0;
  $word->Documents->Open($document, False, True, False, $passwd) or die("Unable to open document");
  //$word->Documents[1]->Saved = 1;
  //$word->ActivePrinter = $printer_name;
  $word->Documents[1]->PrintOut(True, False, 0, $ps_file);
  while($word->BackgroundPrintingStatus > 0){
    sleep(1);
    }
  $word->Documents[1]->Close(false);
  $word->Quit();
  $word = null;
  unset($word);
  while (exec("$gs_inst -sDEVICE=pdfwrite -r300 $out_quality -dNOPAUSE -dBATCH -dSAFER -sPAPERSIZE=a4 -sOutputFile=\"".$pdf_file."\" \"".$ps_file."\"") > 0){
    sleep(1);
    }
  }
if ($_POST['send']){
    print "<pre>";
    if (move_uploaded_file($_FILES['userfile']['tmp_name'], $workdir.$_FILES['userfile']['name'])) {
        if (in_array($suffix, $wordfiles)){
          word($uploaded_doc, $psfile, $pdffile, $password, $gsinst, $printername, $quality);
        }
        elseif (in_array($suffix, $excelfiles)){
          excel($uploaded_doc, $psfile, $pdffile, $gsinst, $printername, $quality);
        }
        elseif (in_array($suffix, $postscriptfiles)){
          while (exec("$gsinst -dBATCH -sDEVICE=pdfwrite -r300 $quality -dNOPAUSE -dBATCH -dSAFER -sPAPERSIZE=a4 -sOutputFile=\"".$pdffile."\" -dNOPAUSE \"$uploaded_doc\"") > 0){
            sleep(1);
            }
        }
        elseif (in_array($suffix, $powerpointfiles)){
          powerpoint($uploaded_doc, $psfile, $pdffile, $gsinst, $printername, $quality);
        } else {
            echo'<strong><font color="#FF0000">Dateiformat wird nicht unterst�tzt !!!</strong></font><br>';
            unlink($uploaded_doc);//Hochgeladene Datei l�schen
            echo'<strong><font color="#FF0000">Hochgeladene Datei wieder gel�scht.</strong></font>';
            exit();
            }
    } else {
        echo '<strong><font color="#FF0000">Datei konnte nicht hochgeladen werden !</strong></font>';
        exit();
        }
    while (!(is_writable($pdffile))){   //abfrage ob Datei verf�gbar ?!
      sleep(1);
    }
    if (!headers_sent()) {
      $header="http://$servername/".$_FILES['userfile']['name'].".pdf";
      header ("Location: $header");
      unlink($psfile);
      unlink($uploaded_doc);
      exit();
      } else { echo 'Das Resultat befindet sich f�r 24 Stunden unter: <a href="http://'.$servername.'/'.$_FILES['userfile']['name'].'.pdf">'.$_FILES['userfile']['name'].'.pdf</a>'; }
    unlink($psfile);//Hochgeladene Datei l�schen
    unlink($uploaded_doc);//Hochgeladene Datei l�schen
    exit();
}
?>
</BODY>
</HTML>