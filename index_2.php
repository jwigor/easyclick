<?
session_start();
if (isset($_REQUEST['login']))
	$_SESSION['login'] = $_REQUEST['login'];
if (isset($_REQUEST['parol']))
	$_SESSION['password'] = md5($_REQUEST['parol']);


require_once ('./conf/db.config.php');
require_once ('./classes/db.class.php');
require_once ('./classes/easyclickorders.class.php');
require_once ('./classes/easyclickordermodels.class.php');
require_once ('./classes/check_login.class.php');
require_once ('./classes/users.class.php');

$E_C_Orders = new EasyClickOrders;
$E_C_O_Models = new EasyClickOrderModels;
$login = new CheckLogin;
$user = new Users;


	

	//echo $login->CheckLoginInfo($_SESSION['login'], $_SESSION['password']);
if ($login->CheckLoginInfo($_SESSION['login'], $_SESSION['password']) == 0)
	header ("Location: login.php");	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--

Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 2.5 License

Title      : Emporium
Version    : 1.0
Released   : 20090222
Description: A two-column, fixed-width and lightweight template ideal for 1024x768 resolutions. Suitable for blogs and small websites.

-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
if (isset($_REQUEST['action']) && ($_REQUEST['action']=='view_samovyvoz' || $_REQUEST['action']=='view_kiev' || $_REQUEST['action']=='view_region'  || $_REQUEST['action']=='view_cancel'  || $_REQUEST['action']=='view_delay' || $_REQUEST['action']=='view_perform') )
echo '<meta HTTP-EQUIV="Refresh" Content="120; URL=index.php?action='.$_REQUEST['action'].'">';
?>

<meta http-equiv="content-type" content="text/html; charset=windows-1251" />
<title>EasyClick :: AdminPanel</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="default.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="calendarview.css">
<link rel="stylesheet" href="calendar.css">
<script type="text/javascript" src="jquery-1.2.6.js"></script>

<script type="text/javascript" src="./jquery_lightbox/js/jquery-1.2.6.pack.js"></script>
		<!-- Include Lightbox (Production) -->
		<script type="text/javascript" src="./jquery_lightbox/js/jquery.lightbox.js"></script>
		<script type="text/javascript">$(function(){
			$.Lightbox.construct({
			text: {
			image: '����',
			close: '�������',
			of: '��'
			}
			});
		});
		</script>

<link type="text/css" rel="stylesheet" media="screen" href="index.css" />
 <style>
    
      div.calendar {
        max-width: 240px;
        margin-left: auto;
        margin-right: auto;
      }
      div.calendar table {
        width: 100%;
      }
      div.dateField {
        width: 140px;
        padding: 6px;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        color: #555;
        background-color: white;
        margin-left: auto;
        margin-right: auto;
        text-align: center;
      }
      div#popupDateField:hover {
        background-color: #cde;
        cursor: pointer;
      }
    </style>
    <script src="prototype.js"></script>
    <script src="calendarview.js"></script>
    <script>
      function setupCalendars() {
        // Embedded Calendar
        Calendar.setup(
          {
            dateField: 'embeddedDateField',
            parentElement: 'embeddedCalendar'
          }
        )

        // Popup Calendar
        Calendar.setup(
          {
            dateField: 'popupDateField',
            triggerElement: 'popupDateField'
          }
        )
      }

      Event.observe(window, 'load', function() { setupCalendars() })
    </script>
	<script language="JavaScript" src="calendar_us.js"></script>
	
</head>

<body>
<div id="wrapper">
<!-- start header -->

<div id="header">
	<div id="menu">
		<ul>
			<strong><li class="current_page_item"><a href="index.php?action=view_samovyvoz">���������</a></li>
			<li><a href="index.php?action=view_kiev">��������</a></li>
			<li><a href="index.php?action=view_region">��������</a></li>
			<li><a href="index.php?action=view_account">�����</a></li>
			<li><a href="index.php?action=view_cancel">������</a></li>
			<li><a href="index.php?action=view_delay">��������</a></li>
			<li><a href="index.php?action=view_perform">�����������</a></li>
			<? if ($_SESSION['admin'] == 'yes'){?>
				<li><a href="index.php?action=view_managers" style="text-decoration:underline;">������</a></li>
			<?}?>
			<li class="last"><a href="login.php?action=exit">�����</a></li></strong>
		</ul>
	</div>
	
	<div id="operations">
		
					<p><h1 class="title"><a href="index.php?action=add_kiev">�������� �� �����</a>&nbsp;|&nbsp;
					<a href="index.php?action=add_region">�������� � ������</a>&nbsp;|&nbsp;
					<a href="index.php?action=add_account">��������� ����</a></h1></p>
		
	</div>

	
</div>
<!-- end header -->
</div>
<!-- start page -->
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			<div class="entry">
			<br><br><br>
				<? if (!isset($_REQUEST['action'])){
				require_once './templates/view_samovyvoz.tpl.php';
				 } else{
				
				switch ($_REQUEST['action'])
				{
					case 'add_kiev':
					require_once './templates/add_kiev.tpl.php';
					break;
					
					case 'add_region':
					require_once './templates/add_region.tpl.php';
					break;
					
					case 'add_account':
					require_once './templates/add_account.tpl.php';
					break;
					
					case 'view_kiev':
					require_once './templates/view_kiev.tpl.php';
					break;
					
					case 'view_region':
					require_once './templates/view_region.tpl.php';
					break;
					
					case 'view_account':
					require_once './templates/view_account.tpl.php';
					break;
					
					case 'view_samovyvoz':
					require_once './templates/view_samovyvoz.tpl.php';
					break;
					
					case 'view_cancel':
					require_once './templates/view_cancel.tpl.php';
					break;
					
					case 'view_perform':
					require_once './templates/view_perform.tpl.php';
					break;
					
					case 'view_delay':
					require_once './templates/view_delay.tpl.php';
					break;
					
					case 'view_managers':
					require_once './templates/view_managers.tpl.php';
					break;
					
					case 'edit_kiev':
					require_once './templates/edit_kiev_2.tpl.php';
					break;
					
					
					case 'edit_region':
					require_once './templates/edit_region.tpl.php';
					break;
					
					case 'edit_account':
					require_once './templates/edit_account.tpl.php';
					break;
					
					case 'delete_kiev':
					require_once './templates/delete_kiev.tpl.php';
					break;
					
					case 'delete_region':
					require_once './templates/delete_region.tpl.php';
					break;
					
					case 'delete_account':
					require_once './templates/delete_account.tpl.php';
					break;
					
					case 'delete_cancel':
					require_once './templates/delete_cancel.tpl.php';
					break;
					
					case 'delete_delay':
					require_once './templates/delete_delay.tpl.php';
					break;
					
				}
				 }?>
			</div>
			<!--<div class="meta">
				<p class="links"><a href="#" class="more">Read full article</a> <b>|</b> <a href="#" class="comments">Comments (32)</a></p>
			</div>
			-->
		</div>
		
	</div>
	<!-- end content -->
	
	<div style="clear: both;">&nbsp;</div>
</div>
<!-- end page -->
<!-- start footer -->
<div id="footer">
	<p id="legal">EasyClick &copy; 2010. ��� ����� ���������. Designed by <a href="#">M.i.G. Web-Studio</a></p>
</div>
<!-- end footer -->
</body>
</html>
<script type="text/javascript">
function SubmitForm()
{
	
	if (
		document.getElementById('product_name').value  !='' && 
		 document.getElementById('currency_type').value  !='' && 
		 document.getElementById('payment_type').value  !='' && 
		 document.getElementById('dostavka_type').value  !='' && 
		 document.getElementById('customer_name').value  !='' && 
		 document.getElementById('address').value  !='' && 
		 document.getElementById('mobile_1').value  !='' 
		 )
		{ 
		 alert('test');
		 document.testform.submit(); 
		}
	 else 
		{
			alert('������� ������������� ����!');
			RETURN false;
		}
		
		
}
</script>

<script type="text/javascript">
function check(obj)
{
	var js_payment_type = 0;
	var js_dostavka_type = 0;
	var js_currency_type = 0;
	var p_type_onoff_val;
	var p_type_val;
	
	var radios = document.getElementsByTagName('input')
	
	for (var i = 0; i < radios.length; i++) 
   { 
      if (radios[i].checked == true && radios[i].name === 'payment_type') 
      { 
        js_payment_type = 1;
		p_type_val = radios[i].value;
      } 
	  
	   if (radios[i].checked == true && radios[i].name === 'dostavka_type') 
      { 
        js_dostavka_type = 1;
      } 
	  
	   if (radios[i].checked == true && radios[i].name === 'currency_type') 
      { 
        js_currency_type = 1;
      } 
	  
	   if (radios[i].checked == true && radios[i].name === 'payment_type_onoff') 
      { 
        p_type_onoff_val = radios[i].value;
      } 
   }
   
   if (obj.product_name.value == '')
	{
		alert ('�� ��������� ���� "�������� ������"');
		return false;
   }
   
   if (obj.price.value == '')
	{
		alert ('�� ��������� ���� "����"');
		return false;
   }
   
     if (js_currency_type == 0) 
   {
		alert ('�� ������� ������');
		return false;
   }
   
   if (js_payment_type == 0) 
   {
		alert ('�� ������� ���� "������"');
		return false;
   }
   
   if (js_dostavka_type == 0) 
   {
		alert ('�� ������� ���� "�������� ����������"');
		return false;
   }
   
    if (obj.customer_name.value=='������� �.�.')
	{
		alert ('�� ��������� ���� "������� �.�."');
		return false;
   }
   
    if (obj.address.value=='�����')
	{
		alert ('�� ��������� ���� "�����"');
		return false;
   }
   
   if (obj.raion.value=='�����')
	{
		alert ('�� ��������� ���� "�����"');
		return false;
   }
  
  if (obj.mobile_1.value=='��������� ������� - 1')
	{
		alert ('�� ��������� ���� "��������� ������� - 1"');
		return false;
   }
   /*
   if (p_type_val == 'beznal' && p_type_onoff_val == '�� ��������' && o_status == 'perform_yes')
	{
		alert (p_type_onoff_val);
		return false;
	}
	*/
obj.submit();
}
</script>

<script type="text/javascript">
function DeleteKiev(order_id)
{
	if (confirm("��������, ������� ������?")) {
	parent.location='index.php?action=delete_kiev&order_id='+order_id;
	}

}
</script>

<script type="text/javascript">
function DeleteRegion(order_id)
{
	if (confirm("��������, ������� ������?")) {
	parent.location='index.php?action=delete_region&order_id='+order_id;
	}

}
</script>

<script type="text/javascript">
function DeleteAccount(order_id)
{
	if (confirm("��������, ������� ������?")) {
	parent.location='index.php?action=delete_account&order_id='+order_id;
	}

}
</script>


<script type="text/javascript">
function DeleteCancel(order_id)
{
	if (confirm("��������, ������� ������?")) {
	parent.location='index.php?action=delete_cancel&order_id='+order_id;
	}

}
</script>

<script type="text/javascript">
function DeleteDelay(order_id)
{
	if (confirm("��������, ������� ������?")) {
	parent.location='index.php?action=delete_delay&order_id='+order_id;
	}

}
</script>

<script language="JavaScript">
<!-- hiden
function openWinMsg(order_id) {
  myWin= open("send_msg.php?order_id=" + order_id , "displayWindow", 
    "width=350,height=300,status=no,toolbar=no,menubar=no,scrollbars=no");
}
// -->
</script>